/**
 * Created by Suhaylee on 16/01/2017.
 */

function numberWithSpaces(x) {
    if(x==null || x==""){
        return x;
    } else{
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
}

$(document).ready(function () {

    $('body').on('keydown', 'input, select, textarea', function(e) {
        var self = $(this)
            , form = self.parents('form:eq(0)')
            , focusable
            , next
            ;
        if (e.keyCode == 13) {
            focusable = form.find('input, a, select, button').filter(':visible');
            next = focusable.eq(focusable.index(this)+1);
            if (next.length) {
                next.focus();
            } else {
                form.submit();
            }
            return false;
        }
    });

});