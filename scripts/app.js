'use strict';

// declare modules
angular.module('Authentication', []);
angular.module('Register', []);
angular.module('Profile', []);
angular.module('Admin', []);


angular.module('Tournaments', []);
angular.module('Participants', []);
angular.module('Clubs', []);
angular.module('Games', []);
angular.module('GamesCurrent', []);
angular.module('Winners', []);
angular.module('Users', []);

angular.module('torgai', [
    'Authentication',
    'Register',

    'ngRoute',
    'ngCookies',
    'ui.grid',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.resizeColumns',
    'ngMaterial',
    'Profile',
    'ngMessages',
    'Admin',
    //Starts here
    'Tournaments',
    'Participants',
    'Clubs',
    'Games',
    'GamesCurrent',
    'Winners',
    'Users'
    ])

.constant('configuration',{
    apiBaseUrl: 'http://localhost:8084/restfull/webresources/'
})

.config(['$routeProvider', '$mdThemingProvider', function ($routeProvider, $mdThemingProvider, $mdIconProvider) {

    $mdThemingProvider.theme('default')
        .primaryPalette('light-blue');

    $routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'modules/authentication/views/login.html',
            hideMenus: true
        })

        .when('/register', {
            controller: 'RegisterController',
            templateUrl: 'modules/register/views/register.html',
            hideMenus: true
        })

        .when('/profile', {
            controller: 'ProfileController',
            templateUrl: 'modules/profile/views/profile.html'
        })

        .when('/admin', {
            controller: 'AdminController',
            templateUrl: 'modules/admin/views/admin.html'
        })

        .when('/tournaments', {
            controller: 'TournamentsController',
            templateUrl: 'modules/tournaments/views/tournaments.html'
        })
        .when('/participants', {
            controller: 'ParticipantsController',
            templateUrl: 'modules/participants/views/participants.html'
        })
        .when('/clubs', {
            controller: 'ClubsController',
            templateUrl: 'modules/clubs/views/clubs.html'
        })
        .when('/games', {
            controller: 'GamesController',
            templateUrl: 'modules/games/views/games.html'
        })
        .when('/gamesCurrent', {
            controller: 'GamesCurrentController',
            templateUrl: 'modules/gamesCurrent/views/games.html'
        })

        .when('/winners', {
            controller: 'WinnersController',
            templateUrl: 'modules/winners/views/winners.html'
        })
        .when('/users', {
            controller: 'UsersController',
            templateUrl: 'modules/users/views/users.html'
        })

        .otherwise({ redirectTo: '/' });
}])

.controller('BasicDemoCtrl', function () {
    var originatorEv;

    this.openMenu = function($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
    }
})

.run(['$rootScope', '$location', '$cookieStore', '$http','i18nService','AuthenticationService',
    function ($rootScope, $location, $cookieStore, $http, i18nService,AuthenticationService) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Token ' + $rootScope.globals.currentUser.authdata;
            $http.defaults.headers.common['Content-Type'] = "application/json; charset=utf-8";
        }

        $rootScope.errorsHandle = function(error){
            console.log("Error Handle");
            console.log(error);
            if(error.status == 401){
                AuthenticationService.logout();
            }
        };

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            i18nService.setCurrentLang('ru');
            if($rootScope.globals.currentUser){
                var tournament = $cookieStore.get('store') || null;

                if(tournament != null){
                    $rootScope.storeName = tournament.name;
                    $rootScope.storeName1 = "- " + tournament.name;
                }
                if(!tournament || tournament == null){
                    $location.path('/tournaments');
                }
            }
            if(!$rootScope.globals.currentUser){
                // redirect to login page if not logged in
                if ($location.path() == '/') {
                    $location.path('/tournaments');
                }
            } else{
                if ($location.path() !== '/login' && $location.path() !== '/register' && !$rootScope.globals.currentUser) {
                    $location.path('/login');
                } else if ($location.path() == '/' && $rootScope.globals.currentUser) {
                    $location.path('/participants');
                } else if ($location.path() == '/login' && $rootScope.globals.currentUser) {
                    $location.path('/participants');
                }
            }
        });
    }]);