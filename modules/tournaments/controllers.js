'use strict';


angular.module('Tournaments', [
        'ngTouch',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.selection'
    ])

    .controller('TournamentsController', ['$scope', '$http', 'configuration', '$mdDialog', '$rootScope', '$cookieStore', 'uiGridConstants', '$timeout',
        function ($scope, $http, configuration, $mdDialog, $rootScope, $cookieStore, uiGridConstants, $timeout) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'tournaments/';
            var storeId = '';

            $scope.storesList = {};
            $scope.onlyNumbers = /^\d+$/;
            $scope.loadingGridOptions = true;

            var role = $cookieStore.get('globals') || null;
            if(role!=null){
                role = role.currentUser.role;
            }
            var selectedStore = '';

            $scope.checker = true;
            $scope.checkerArchieve = true;
            $scope.checkerStart = true;


            if (role === "ADMIN") {
                $scope.admin = true;
                $scope.owner = false;

            } else if (role === "OWNER"){
                $scope.admin = false;
                $scope.owner = true;
            }

            $scope.checkRole = false;
            if (role === "ADMIN") {
                $scope.checkRole = true;
            } else if (role === "OWNER"){
                $scope.checkRole = false;
            }

            $scope.addGood = function () {
                var dataToPass = {};
                $mdDialog.show({
                    locals: {dataToPass: dataToPass},
                    controller: DialogController,
                    templateUrl: 'modules/tournaments/views/add-modal.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen
                });
            };

            $rootScope.buttonsClick = function (ev, type, row) {
                var dataToPass = row.entity;

                if (type == 1) {
                    $mdDialog.show({
                        locals: {dataToPass: dataToPass},
                        controller: DialogController,
                        templateUrl: 'modules/tournaments/views/display-modal.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen
                    });
                } else if (type == 2) {
                    $mdDialog.show({
                            locals: {dataToPass: dataToPass},
                            controller: DialogController,
                            templateUrl: 'modules/tournaments/views/update-modal.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: false,
                            fullscreen: $scope.customFullscreen
                        })
                        .then(function (answer) {
                        }, function () {
                            doRequest();
                        });
                } else {
                    var confirm = $mdDialog.confirm()
                        .title('Вы уверены, что хотите удалить турнир "' + dataToPass.name + '"?')
                        .textContent('После удаления Вы не сможете его вернуть.')
                        .targetEvent(ev)
                        .ok('Отмена')
                        .cancel('Удалить');

                    $mdDialog.show(confirm).then(function () {
                    }, function () {
                        $http({
                            method: 'DELETE',
                            url: baseUrl + objectName + "/" + dataToPass.id
                        }).then(function () {
                            var index = $scope.gridOptions.data.indexOf(row.entity);
                            $scope.gridOptions.data.splice(index, 1);
                        }).catch(function (response) {
                            $rootScope.errorsHandle(response);
                        });
                    });
                }
            };

            function DialogController($scope, $mdDialog, dataToPass) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    if(answer == "editFromDisplay"){
                        var r = {};
                        r.entity = dataToPass;
                        $rootScope.buttonsClick(null, 2, r);
                    } else if (answer == 'update') {
                        $http({
                            method: 'PUT',
                            url: baseUrl + objectName + "/" + $scope.choosenGood.id,
                            data: $scope.choosenGood
                        }).then(function (data) {
                            console.log("Good update success: " + data);
                        }).catch(function (response) {
                            console.log("Good update error: " + response);
                            $rootScope.errorsHandle(response);
                        });
                    } else if (answer == 'not update') {
                        doRequest();
                    } else if (answer == 'add') {
                        console.log($scope.addingGood);
                        $http({
                            method: 'POST',
                            url: baseUrl + objectName,
                            data: $scope.addingGood
                        }).then(function (data) {
                            console.log("Good adding success: " + data);
                            doRequest();
                        }).catch(function (response) {
                            console.log("Good adding error: " + response);
                            $rootScope.errorsHandle(response);
                        });
                    }
                    $mdDialog.hide(answer);
                };
                $scope.choosenGood = dataToPass;
                $scope.addingGood = dataToPass;
            }

            $scope.gridOptions = {
                enableFiltering: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                multiSelect: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                columnDefs: [
                    {
                        field: 'name',
                        displayName: 'Название'
                    },
                    {
                        field: 'status',
                        displayName: 'Статус'
                    },
                    {
                        name: ' ',
                        width: 50,
                        cellTemplate: //'<span class="goodsIconButtons goodsIconButtonsSee glyphicon glyphicon-eye-open" ng-click="grid.appScope.buttonsClick($event, 1, row)"></span>' +
                        '<span class="goodsIconButtons glyphicon glyphicon-pencil" ng-show="row.entity.changeable" ng-click="grid.appScope.buttonsClick($event, 2, row)"></span>' +
                        '<span class="goodsIconButtons glyphicon glyphicon-trash" ng-show="row.entity.changeable" ng-click="grid.appScope.buttonsClick($event, 3, row)"></span>',
                        enableFiltering: false,
                        enableSorting: false,
                        enableColumnMenu: false
                    }
                ]
            };
            $scope.gridOptions.onRegisterApi = function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.checker = false;
                    if(row.entity.status == "Новый"){
                        $scope.checkerArchieve = true;
                        $scope.checkerStart = false;
                    } else if(row.entity.status == "Начато"){
                        $scope.checkerArchieve = true;
                        $scope.checkerStart = true;
                    } else if(row.entity.status === "Закончен"){
                        $scope.checkerStart = true;
                        $scope.checkerArchieve = false;
                    } else if(row.entity.status === "Заархивирован"){
                        $scope.checkerStart = true;
                        $scope.checkerArchieve = true;
                    }
                    selectedStore = row.entity;
                });
            };

            var doRequest = function () {
                $scope.loadingGridOptions = false;

                $http({
                    method: 'GET',
                    url: baseUrl + objectName
                }).then(function (response) {
                    var data = response.data;
                    console.log(data);
                    for(var i=0;i<data.length;i++){
                        data[i].changeable = false;
                        if(data[i].status == 'Новый' && $scope.admin){
                            data[i].changeable = true;
                        }
                    }
                    $scope.gridOptions.data = data;
                    $scope.loadingGridOptions = true;
                }).catch(function (response) {
                    $rootScope.errorsHandle(response);
                    $scope.loadingGridOptions = true;
                });
            };
            doRequest();

            $scope.chooseTournament = function() {
                $cookieStore.put('store', selectedStore);
                if(selectedStore.status == "Закончен") {
                    location.href = "#/winners";
                } else if(selectedStore.status == "Заархивирован"){
                    location.href = "#/games";
                } else if(selectedStore.status == "Новый"){
                    location.href = "#/participants";
                } else{
                    location.href = "#/gamesCurrent";
                }
            };
            $scope.archieveTournament = function () {
                var confirm = $mdDialog.confirm()
                    .title('Вы уверены, что хотите заархивировать турнир "' + selectedStore.name + '"?')
                    .targetEvent()
                    .ok('Отмена')
                    .cancel('Заархивировать');

                $mdDialog.show(confirm).then(function () {
                }, function () {
                    selectedStore.status = "archived";
                    $http({
                        method: 'PUT',
                        url: baseUrl + objectName + "/" + selectedStore.id,
                        data: selectedStore
                    }).then(function (data) {
                        console.log("Good update success: " + data);
                        selectedStore.status = "Заархивирован";
                        $cookieStore.put('store', selectedStore);
                        location.href = "#/games";
                    }).catch(function (response) {
                        console.log("Good update error: " + response);
                        $rootScope.errorsHandle(response);
                    });
                });
            };
            $scope.startTournament = function() {
                var confirm = $mdDialog.confirm()
                    .title('Вы уверены, что хотите начать турнир "' + selectedStore.name + '"?')
                    .textContent('После Вы не сможете изменить или добавить участников в нем.')
                    .targetEvent()
                    .ok('Отмена')
                    .cancel('Начать');

                $mdDialog.show(confirm).then(function () {
                }, function () {
                    selectedStore.status = "started";
                    $http({
                        method: 'PUT',
                        url: baseUrl + objectName + "/" + selectedStore.id,
                        data: selectedStore
                    }).then(function (data) {
                        console.log("Good update success: " + data);
                        selectedStore.status = "Начато";
                        $cookieStore.put('store', selectedStore);
                        $http({
                            method: 'POST',
                            url: baseUrl + "games/" + selectedStore.id + "/startNextTour"
                        }).then(function () {
                            console.log("started next tour");
                            location.href = "#/gamesCurrent";
                        }).catch(function (response) {
                            $rootScope.errorsHandle(response);
                        });
                    }).catch(function (response) {
                        console.log("Good update error: " + response);
                        $rootScope.errorsHandle(response);
                    });
                });
            }
        }]);
