'use strict';

angular.module('Register')

.controller('RegisterController',
    ['$scope', '$rootScope', '$location', 'RegisterService',
    function ($scope, $rootScope, $location, RegisterService){

        $scope.Register = function() {

            var user = {
                name: $scope.name,
                surname: $scope.surname,
                login: $scope.username,
                password: sha256($scope.password)
            };

            RegisterService.Register(user, function(response) {
                if(response.success) {
                    console.log(response);
                    $scope.checkState = false;
                    $location.path('/stores_choice');
                } else {
                    if(response.status == -1){
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: "Ошибка сервера. Попробуйте запрос попозже."
                        },{
                            element: 'body',
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: false,
                            showProgressbar: false,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            offset: 10,
                            spacing: 10,
                            z_index: 1031,
                            delay: 2000,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    } else{
                        $scope.checkState = true;
                        $scope.error = response.message;
                    }
                    $scope.dataLoading = false;
                }
            });
        };

    }]);
