'use strict';


angular.module('Participants', [
        'ngTouch',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.selection',
        'ui.grid.edit',
        'ui.grid.cellNav'
    ])

    .controller('ParticipantsController', ['$scope', '$http', 'configuration', '$mdDialog', '$rootScope', '$cookieStore', 'uiGridConstants', '$timeout',
        function ($scope, $http, configuration, $mdDialog, $rootScope, $cookieStore, uiGridConstants, $timeout) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'participant/';
            var storeId = '';
            var sexx = ["Мужской","Женский"];
            var settings = $cookieStore.get('settings') || {};
            var store = $cookieStore.get('store') || {};
            var clubs = [];

            var role = $cookieStore.get('globals') || null;
            if(role!=null){
                role = role.currentUser.role;
            }
            if (role === "ADMIN") {
                $scope.admin = true;
                $scope.judge = false;
                $rootScope.admin = true;
                $rootScope.judge = false;
            } else if (role === "JUDGE"){
                $scope.admin = false;
                $scope.judge = true;
                $rootScope.admin = false;
                $rootScope.judge = true;
            } else{
                $scope.admin = false;
                $scope.judge = false;
                $rootScope.admin = false;
                $rootScope.judge = false;
            }
            $scope.checkStatus = false;
            if(store.status == "Новый" && $scope.admin){
                $scope.checkStatus = true;
            }

            $scope.storesList = {};
            $scope.onlyNumbers = /^\d+$/;
            $scope.loadingGridOptions = true;

            $scope.addGood = function () {
                var dataToPass = {};
                dataToPass.sexx = sexx;
                dataToPass.clubs = clubs;
                if(dataToPass.clubs == []){
                    $http({
                        method: 'GET',
                        url: baseUrl + "club"
                    }).then(function (response) {
                        var data = response.data;
                        dataToPass.clubs = data;
                        $mdDialog.show({
                            locals: {dataToPass: dataToPass},
                            controller: DialogController,
                            templateUrl: 'modules/participants/views/add-modal.html',
                            parent: angular.element(document.body),
                            clickOutsideToClose: false,
                            fullscreen: $scope.customFullscreen
                        });
                    }).catch(function (response) {
                        $rootScope.errorsHandle(response);
                        $scope.loadingGridOptions = true;
                    });
                } else{
                    $mdDialog.show({
                        locals: {dataToPass: dataToPass},
                        controller: DialogController,
                        templateUrl: 'modules/participants/views/add-modal.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen
                    });
                }
            };

            $scope.importGoods = function () {
                var dataToPass = {};
                dataToPass.clubs = clubs;
                $mdDialog.show({
                    locals: {dataToPass: dataToPass},
                    controller: DialogController,
                    templateUrl: 'modules/participants/views/import-modal.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen
                });
            };

            $rootScope.buttonsClick = function (ev, type, row) {
                var dataToPass = row.entity;
                console.log(row);
                dataToPass.sexx = sexx;
                dataToPass.clubs = clubs;

                if (type == 1) {
                    $mdDialog.show({
                        locals: {dataToPass: dataToPass},
                        controller: DialogController,
                        templateUrl: 'modules/participants/views/display-modal.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen
                    });
                } else if (type == 2) {
                    $mdDialog.show({
                            locals: {dataToPass: dataToPass},
                            controller: DialogController,
                            templateUrl: 'modules/participants/views/update-modal.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: false,
                            fullscreen: $scope.customFullscreen
                        })
                        .then(function (answer) {
                        }, function () {
                            doRequest();
                        });
                } else {
                    var confirm = $mdDialog.confirm()
                        .title('Вы уверены, что хотите удалить товар "' + dataToPass.goodCaption + '"?')
                        .textContent('После удаления Вы не сможете его вернуть.')
                        .targetEvent(ev)
                        .ok('Отмена')
                        .cancel('Удалить');

                    $mdDialog.show(confirm).then(function () {
                    }, function () {
                        $http({
                            method: 'DELETE',
                            url: baseUrl + objectName + "/" + dataToPass.id
                        }).then(function () {
                            doRequest();
                        }).catch(function (response) {
                            $rootScope.errorsHandle(response);
                        });
                    });
                }
            };

            function DialogController($scope, $mdDialog, dataToPass) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    if(answer == "editFromDisplay"){
                        var r = {};
                        r.entity = dataToPass;
                        $rootScope.buttonsClick(null, 2, r);
                    } else if (answer == 'update') {
                        for(var i=0;i<clubs.length;i++){
                            if(clubs[i].name == $scope.choosenGood.club){
                                $scope.choosenGood.club = clubs[i].id;
                            }
                        }
                        $http({
                            method: 'PUT',
                            url: baseUrl + objectName + "/" + $scope.choosenGood.id,
                            data: $scope.choosenGood
                        }).then(function (data) {
                            console.log("Good update success: " + data);
                            $rootScope.refreshTable();
                            doRequest();
                        }).catch(function (response) {
                            console.log("Good update error: " + response);
                            doRequest();
                            $rootScope.errorsHandle(response);
                        });
                    } else if (answer == 'not update') {
                        doRequest();
                    } else if (answer == 'add') {
                        console.log($scope.choosenGood);
                        var row = {};
                        row.data = [];
                        row.data[0] = $scope.choosenGood;
                        console.log(row);
                        $http({
                            method: 'POST',
                            url: baseUrl + objectName + storeId,
                            data: row
                        }).then(function (data) {
                            console.log("Good adding success: " + data);
                            doRequest();
                        }).catch(function (response) {
                            console.log("Good adding error: " + response);
                            $rootScope.errorsHandle(response);
                        });
                    }  else if (answer == 'import') {
                        $rootScope.makeImport(dataToPass.importClub);
                    }
                    $mdDialog.hide(answer);
                };
                $scope.choosenGood = dataToPass;
                $scope.addingGood = dataToPass;
            }
            $scope.rowDblClick = function( row) {
                $rootScope.buttonsClick(null, 1, row);
            };

            $rootScope.gridOptions = {
                enableFiltering: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                multiSelect: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                enableColumnResizing: true,
                columnDefs: [
                    {
                        field: 'name',
                        displayName: 'ФИО',
                        cellTemplate: '<div class="ui-grid-cell-contents"> {{row.entity.name}} {{row.entity.surname}} {{row.entity.patronymic}} </div>',
                        enableCellEdit: false
                    },
                    {
                        field: 'weight',
                        displayName: 'Вес',
                        enableFiltering: false,
                        width: 100,
                        enableCellEdit: $scope.checkStatus
                    },
                    {
                        field: 'age',
                        displayName: 'Возраст',
                        width: 124,
                        enableCellEdit: false
                    },
                    {
                        field: 'sex',
                        displayName: 'Пол',
                        width: 76,
                        enableCellEdit: false
                    },
                    {
                        field: 'rating',
                        displayName: 'КЮ',
                        enableFiltering: false,
                        width: 50,
                        enableCellEdit: false
                    },
                    {
                        field: 'city',
                        displayName: 'Город',
                        enableFiltering: false,
                        width: 100,
                        enableCellEdit: false
                    },
                    {
                        field: 'country',
                        displayName: 'Страна',
                        enableFiltering: false,
                        width: 100,
                        enableCellEdit: false
                    },
                    {
                        field: 'club',
                        displayName: 'Клуб',
                        enableFiltering: false,
                        width: 100,
                        enableCellEdit: false
                    },
                    {
                        name: ' ',
                        width: 75,
                        cellTemplate: '<span class="goodsIconButtons goodsIconButtonsSee glyphicon glyphicon-eye-open" ng-click="grid.appScope.buttonsClick($event, 1, row)"></span>' +
                        '<span class="goodsIconButtons glyphicon glyphicon-pencil" ng-show="grid.appScope.checkStatus" ng-click="grid.appScope.buttonsClick($event, 2, row)"></span>' +
                        '<span class="goodsIconButtons glyphicon glyphicon-trash" ng-show="grid.appScope.checkStatus" ng-click="grid.appScope.buttonsClick($event, 3, row)"></span>',
                        enableFiltering: false,
                        enableSorting: false,
                        enableColumnMenu: false,
                        enableCellEdit: false
                    }
                ],
            };
            $rootScope.gridOptions.onRegisterApi = function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    console.log(rowEntity);
                    for(var i=0;i<clubs.length;i++){
                        if(clubs[i].name == rowEntity.club){
                            rowEntity.club = clubs[i].id;
                        }
                    }
                    $http({
                        method: 'PUT',
                        url: baseUrl + objectName + rowEntity.id,
                        data: rowEntity
                    }).then(function (data) {
                        console.log("Good update success: " + data);
                        doRequest();
                    }).catch(function (response) {
                        console.log("Good update error: " + response);
                        $rootScope.errorsHandle(response);
                        doRequest();
                    });
                });
            };

            var doRequest = function () {
                if (storeId != '') {
                    console.log("her1 refreshing");
                    $scope.loadingGridOptions = false;
                    $http({
                        method: 'GET',
                        url: baseUrl + objectName + storeId
                    }).then(function (response) {
                        var data = response.data;
                        console.log(data);
                        console.log("here goes");
                        console.log($rootScope.gridOptions.data);
                        $rootScope.gridOptions.data = [];
                        $rootScope.gridOptions.data = data;
                        $scope.loadingGridOptions = true;
                    }).catch(function (response) {
                        $rootScope.errorsHandle(response);
                        $scope.loadingGridOptions = true;
                    });
                }
            };

            var getPage = function () {
                $http({
                    method: 'GET',
                    url: baseUrl + "club"
                }).then(function (response) {
                    var data = response.data;
                    clubs = data;
                    doRequest();
                }).catch(function (response) {
                    $rootScope.errorsHandle(response);
                    $scope.loadingGridOptions = true;
                });
            };

            var changeStore = function () {
                storeId = store.id;
                if (storeId != '') {
                    $(".storesLabel").removeAttr("style");
                }

                if (storeId.length == 0) {
                    $scope.checker = true;
                } else {
                    $scope.checker = false;
                }

                getPage();
            };

            changeStore();

            $scope.importParticipants = {};

            $scope.reset = function() {
                $scope.importParticipants.data = [];
                $scope.importParticipants.columnDefs = [];
            }
            $rootScope.makeImport = function(importClub) {
                console.log($scope.importParticipants.data);
                console.log(importClub);
                var row = {};
                row.data = $scope.importParticipants.data;
                for(var i=0;i<row.data.length;i++){
                    row.data[i].club = importClub;
                }
                console.log(row);
                $http({
                    method: 'POST',
                    url: baseUrl + objectName + storeId,
                    data: row
                }).then(function (data) {
                    console.log("Good adding success: " + data);
                    $rootScope.refreshTable();
                }).catch(function (response) {
                    console.log("Good adding error: " + response);
                    $rootScope.errorsHandle(response);
                });
            };

            $rootScope.refreshTable = function(){
                doRequest();
            }

        }])

    .directive("fileread", [function () {
        return {
            scope: {
                opts: '='
            },
            link: function ($scope, $elm, $attrs) {
                $elm.on('change', function (changeEvent) {
                    var reader = new FileReader();

                    reader.onload = function (evt) {
                        $scope.$apply(function () {
                            var data = evt.target.result;

                            var workbook = XLSX.read(data, {type: 'binary'});

                            var headerNames = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];

                            var data = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]]);

                            $scope.opts.columnDefs = [];
                            headerNames.forEach(function (h) {
                                $scope.opts.columnDefs.push({ field: h });
                            });

                            $scope.opts.data = data;

                            $elm.val(null);
                        });
                    };

                    reader.readAsBinaryString(changeEvent.target.files[0]);
                });
            }
        }
    }]);
