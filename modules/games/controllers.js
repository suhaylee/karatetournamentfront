'use strict';


angular.module('Games', [
        'ngTouch',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.selection',
        'ui.grid.edit',
        'ui.grid.cellNav',
        'ui.grid.exporter'
    ])

    .controller('GamesController', ['$scope', '$http', 'configuration', '$mdDialog', '$rootScope', '$cookieStore', 'uiGridConstants', '$timeout',
        function ($scope, $http, configuration, $mdDialog, $rootScope, $cookieStore, uiGridConstants, $timeout) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'games/';
            var storeId = '';
            var sexx = ["Мужской","Женский"];
            var settings = $cookieStore.get('settings') || {};
            var store = $cookieStore.get('store') || {};
            var clubs = [];

            $scope.checkStatus = false;
            if(store.status == "Новый"){
                $scope.checkStatus = true;
            }

            $scope.storesList = {};
            $scope.onlyNumbers = /^\d+$/;
            $scope.loadingGridOptions = true;

            $rootScope.buttonsClick = function (ev, type, row) {
                var dataToPass = row.entity;
                console.log(row);
                dataToPass.sexx = sexx;
                dataToPass.clubs = clubs;

                if (type == 1) {
                    $mdDialog.show({
                        locals: {dataToPass: dataToPass},
                        controller: DialogController,
                        templateUrl: 'modules/games/views/display-modal.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen
                    });
                }
            };

            function DialogController($scope, $mdDialog, dataToPass) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };
                $scope.choosenGood = dataToPass;
            }

            var tours = [];

            $scope.gridOptions = {
                enableFiltering: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                multiSelect: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                enableColumnResizing: true,
                columnDefs: [
                    {
                        field: 'participant1',
                        displayName: 'Участник 1',
                        //cellTemplate: '<div class="ui-grid-cell-contents"> {{row.entity.participantByParticipant1.name}} {{row.entity.participantByParticipant1.surname}} {{row.entity.participantByParticipant1.patronymic}} </div>',
                        enableCellEdit: false
                    },
                    {
                        field: 'participant2',
                        displayName: 'Участник 2',
                        //cellTemplate: '<div class="ui-grid-cell-contents"> {{row.entity.participantByParticipant2.name}} {{row.entity.participantByParticipant2.surname}} {{row.entity.participantByParticipant2.patronymic}} </div>',
                        enableCellEdit: false
                    },
                    {
                        name: 'winner',
                        displayName: 'Победитель',
                        width: '20%',
                        enableCellEdit: false
                    },
                    {
                        field: 'comment',
                        displayName: 'Комментарий',
                        width: 160,
                        enableCellEdit: false
                    },
                    {
                        field: 'weightCategory',
                        displayName: 'Весовая категория',
                        width: 160,
                        enableCellEdit: false
                    },
                    {
                        field: 'tour',
                        displayName: 'Пулька',
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: tours
                        },
                        width: 100,
                        enableCellEdit: false
                    },
                    {
                        field: 'participantByParticipant1.sex',
                        displayName: 'Пол',
                        width: 120,
                        enableCellEdit: false,
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [ { value: 'Мужской', label: 'Мужской' }, { value: 'Женский', label: 'Женский' } ]
                        }
                    },
                    {
                        name: ' ',
                        width: 25,
                        cellTemplate: '<span class="goodsIconButtons goodsIconButtonsSee glyphicon glyphicon-eye-open" ng-click="grid.appScope.buttonsClick($event, 1, row)"></span>',
                        enableFiltering: false,
                        enableSorting: false,
                        enableColumnMenu: false,
                        enableCellEdit: false
                    }
                ],
                enableGridMenu: true,
                exporterCsvFilename: 'myFile.csv',
                exporterPdfDefaultStyle: {fontSize: 9},
                exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                exporterPdfFooter: function ( currentPage, pageCount ) {
                    return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                },
                exporterPdfCustomFormatter: function ( docDefinition ) {
                    docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                    docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                    return docDefinition;
                },
                exporterPdfOrientation: 'portrait',
                exporterPdfPageSize: 'LETTER',
                exporterPdfMaxGridWidth: 500,
                exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                onRegisterApi: function(gridApi){
                    $scope.gridApi = gridApi;
                }
            };
            $scope.gridOptions.onRegisterApi = function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    console.log(rowEntity);
                    for(var i=0;i<clubs.length;i++){
                        if(clubs[i].name == rowEntity.club){
                            rowEntity.club = clubs[i].id;
                        }
                    }
                    $http({
                        method: 'PUT',
                        url: baseUrl + objectName + rowEntity.id,
                        data: rowEntity
                    }).then(function (data) {
                        console.log("Good update success: " + data);
                        doRequest();
                    }).catch(function (response) {
                        console.log("Good update error: " + response);
                        $rootScope.errorsHandle(response);
                        doRequest();
                    });
                });
            };

            var doRequest = function () {
                console.log("here refreshing");
                if (storeId != '') {
                    console.log("her1 refreshing");
                    $scope.loadingGridOptions = false;
                    $http({
                        method: 'GET',
                        url: baseUrl + objectName + storeId
                    }).then(function (response) {
                        var data = response.data;
                        console.log(data);
                        var used = [], cnt=0;
                        for(var i=0;i<data.length;i++){
                            if(used[data[i].tour]!=1){
                                used[data[i].tour]=1;
                                tours.push({value: data[i].tour, label:data[i].tour});
                            }
                            data[i].weightCategory = data[i].participantByParticipant1.weight/3 * 3 + "кг - " + (data[i].participantByParticipant1.weight/3 * 3 + 2) + "кг";
                            if(data[i].winner == 1){
                                data[i].winner = "Участник 1";
                            } else if(data[i].winner == 2){
                                data[i].winner = "Участник 2";
                            } else if(data[i].winner == 3){
                                data[i].winner = "Дисквалификация";
                            } else{
                                data[i].winner = "";
                            }
                            data[i].participant1 = data[i].participantByParticipant1.name + " " +
                                data[i].participantByParticipant1.surname + " " +
                                data[i].participantByParticipant1.patronymic;
                            if(data[i].participantByParticipant2!=null){
                                data[i].participant2 = data[i].participantByParticipant2.name + " " +
                                    data[i].participantByParticipant2.surname + " " +
                                    data[i].participantByParticipant2.patronymic;
                            }
                            if(data[i].participantByParticipant1.sex == "male" || data[i].participantByParticipant1.sex == "Male" || data[i].participantByParticipant1.sex == "MALE"){
                                data[i].participantByParticipant1.sex = "Мужской";
                            } else if(data[i].participantByParticipant1.sex == "female" || data[i].participantByParticipant1.sex == "Female" || data[i].participantByParticipant1.sex == "FEMALE"){
                                data[i].participantByParticipant1.sex = "Женский";
                            }
                        }
                        console.log(tours);
                        $scope.gridOptions.data = data;
                        $scope.loadingGridOptions = true;
                    }).catch(function (response) {
                        $rootScope.errorsHandle(response);
                        $scope.loadingGridOptions = true;
                    });
                }
            };

            var getPage = function () {
                doRequest();
            };

            var changeStore = function () {
                storeId = store.id;
                if (storeId != '') {
                    $(".storesLabel").removeAttr("style");
                }

                if (storeId.length == 0) {
                    $scope.checker = true;
                } else {
                    $scope.checker = false;
                }

                getPage();
            };

            changeStore();

            $scope.importParticipants = {};

            $scope.reset = function() {
                $scope.importParticipants.data = [];
                $scope.importParticipants.columnDefs = [];
            }
            $rootScope.makeImport = function(importClub) {
                console.log($scope.importParticipants.data);
                console.log(importClub);
                var row = {};
                row.data = $scope.importParticipants.data;
                for(var i=0;i<row.data.length;i++){
                    row.data[i].club = importClub;
                }
                console.log(row);
                $http({
                    method: 'POST',
                    url: baseUrl + objectName + storeId,
                    data: row
                }).then(function (data) {
                    console.log("Good adding success: " + data);
                    $scope.refreshTable();
                }).catch(function (response) {
                    console.log("Good adding error: " + response);
                    $rootScope.errorsHandle(response);
                });
            };

            $scope.refreshTable = function(){
                console.log("refreshing");
                doRequest();
            }

        }]);