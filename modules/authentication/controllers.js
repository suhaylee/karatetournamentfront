'use strict';

angular.module('Authentication')

.controller('LogoutController',
    ['$scope','configuration', '$http', '$location', 'AuthenticationService',
    function ($scope, configuration, $http, $location, AuthenticationService){

      $scope.logout = function() {
        AuthenticationService.logout();
        $location.path('#/tournaments');
      };
}])

.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService){
        // reset login status
        AuthenticationService.ClearCredentials();

        $scope.username = "admin";
        $scope.password = "admin";

        $scope.checkState = false;

        $scope.login = function() {

            $scope.dataLoading = true;

            $rootScope.lastLocation = '#/goods';

            AuthenticationService.Login($scope.username, sha256($scope.password), function(response) {
                if(response.success) {
                    $scope.checkState = false;
                    AuthenticationService.SetCredentials($scope.username, sha256($scope.password), response);
                    $location.path('/stores_choice');
                } else {
                    if(response.status == -1){
                        $.notify({
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: "Ошибка сервера. Попробуйте запрос попозже."
                        },{
                            element: 'body',
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: false,
                            showProgressbar: false,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            offset: 10,
                            spacing: 10,
                            z_index: 1031,
                            delay: 2000,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    } else{
                        $scope.checkState = true;
                        $scope.error = response.message;
                    }
                    $scope.dataLoading = false;
                }
            });
        };

    }]);
