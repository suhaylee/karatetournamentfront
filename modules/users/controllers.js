'use strict';


angular.module('Users', [
        'ngTouch',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.selection',
        'ui.grid.edit'
    ])

    .controller('UsersController', ['$scope', '$http', 'configuration', '$mdDialog', '$rootScope', '$cookieStore', 'uiGridConstants', '$timeout',
        function ($scope, $http, configuration, $mdDialog, $rootScope, $cookieStore, uiGridConstants, $timeout) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'user/';
            var storeId = '';

            $scope.storesList = {};
            $scope.onlyNumbers = /^\d+$/;
            $scope.loadingGridOptions = true;

            var role = $rootScope.globals.currentUser.role;
            var selectedStore = '';

            $scope.checker = true;
            $scope.checkerArchieve = true;
            $scope.checkerStart = true;


            if (role === "ADMIN") {
                $scope.admin = true;
                $scope.owner = false;

            } else if (role === "OWNER"){
                $scope.admin = false;
                $scope.owner = true;
            }

            $scope.checkRole = false;
            if (role === "ADMIN") {
                $scope.checkRole = true;
            } else if (role === "OWNER"){
                $scope.checkRole = false;
            }

            $scope.gridOptions = {
                enableFiltering: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                multiSelect: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                columnDefs: [
                    {
                        field: 'login',
                        displayName: 'Логин',
                        enableCellEdit: false
                    },
                    {
                        field: 'name',
                        displayName: 'Имя',
                        enableCellEdit: false
                    },
                    {
                        field: 'surname',
                        displayName: 'Фамилия',
                        enableCellEdit: false
                    },
                    {
                        name: 'role',
                        displayName: 'Рол',
                        editableCellTemplate: 'ui-grid/dropdownEditor',
                        editDropdownValueLabel: 'winner',
                        editDropdownOptionsArray: [
                            { id: "ADMIN", winner: 'Админ' },
                            { id: "JUDGE", winner: 'Судья' },
                            { id: "USER", winner: 'Пользователь' }
                        ]
                    }
                ]
            };
            $scope.gridOptions.onRegisterApi = function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    if(rowEntity.role == 'Админ'){
                        rowEntity.role = 'ADMIN';
                    } else if(rowEntity.role == 'Судья'){
                        rowEntity.role = 'JUDGE';
                    } else if(rowEntity.role == 'Пользователь'){
                        rowEntity.role = 'USER';
                    }
                    $http({
                        method: 'PUT',
                        url: baseUrl + objectName + rowEntity.id,
                        data: rowEntity
                    }).then(function (data) {
                        console.log("Good update success: " + data);
                        doRequest();
                    }).catch(function (response) {
                        console.log("Good update error: " + response);
                        $rootScope.errorsHandle(response);
                        doRequest();
                    });
                });

            };

            var doRequest = function () {
                $scope.loadingGridOptions = false;

                $http({
                    method: 'GET',
                    url: baseUrl + objectName
                }).then(function (response) {
                    var data = response.data;
                    console.log(data);
                    for(var i=0;i<data.length;i++){
                        data[i].changeable = false;
                        if(data[i].role == 'ADMIN'){
                            data[i].role = 'Админ';
                        } else if(data[i].role == 'JUDGE'){
                            data[i].role = 'Судья';
                        } else if(data[i].role == 'USER'){
                            data[i].role = 'Пользователь';
                        }
                    }
                    $scope.gridOptions.data = data;
                    $scope.loadingGridOptions = true;
                }).catch(function (response) {
                    $rootScope.errorsHandle(response);
                    $scope.loadingGridOptions = true;
                });
            };
            doRequest();
        }]);
