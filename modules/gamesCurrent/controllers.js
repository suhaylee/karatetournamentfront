'use strict';


angular.module('GamesCurrent', [
        'ngTouch',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.selection',
        'ui.grid.edit',
        'ui.grid.cellNav',
        'ui.grid.exporter'
    ])

    .controller('GamesCurrentController', ['$scope', '$http', 'configuration', '$mdDialog', '$rootScope', '$cookieStore', 'uiGridConstants', '$timeout',
        function ($scope, $http, configuration, $mdDialog, $rootScope, $cookieStore, uiGridConstants, $timeout) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'games/';
            var storeId = '';
            var sexx = ["Мужской","Женский"];
            var settings = $cookieStore.get('settings') || {};
            var store = $cookieStore.get('store') || {};
            var clubs = [];

            $scope.checkStatus = true;
            /*if(store.statuses == "Начато"){
                $scope.finishShow = true;
            } else{
                $scope.finishShow = false;
            }*/

            var role = $cookieStore.get('globals') || null;
            if(role!=null){
                role = role.currentUser.role;
            }
            if (role === "ADMIN") {
                $scope.admin = true;
                $scope.judge = false;
            } else if (role === "JUDGE"){
                $scope.admin = false;
                $scope.judge = true;
            } else{
                $scope.admin = false;
                $scope.judge = false;
            }

            $scope.storesList = {};
            $scope.onlyNumbers = /^\d+$/;
            $scope.loadingGridOptions = true;

            $rootScope.buttonsClick = function (ev, type, row) {
                var dataToPass = row.entity;
                console.log(row);
                dataToPass.sexx = sexx;
                dataToPass.clubs = clubs;

                if (type == 1) {
                    $mdDialog.show({
                        locals: {dataToPass: dataToPass},
                        controller: DialogController,
                        templateUrl: 'modules/GamesCurrent/views/display-modal.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen
                    });
                }
            };

            function DialogController($scope, $mdDialog, dataToPass) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };
                $scope.choosenGood = dataToPass;
            }

            var tours = [];

            console.log("judge: " + $scope.judge);

            $scope.gridOptions = {
                enableFiltering: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                multiSelect: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                enableColumnResizing: true,
                columnDefs: [
                    {
                        field: 'participant1',
                        displayName: 'Участник 1',
                        //cellTemplate: '<div class="ui-grid-cell-contents"> {{row.entity.participantByParticipant1.name}} {{row.entity.participantByParticipant1.surname}} {{row.entity.participantByParticipant1.patronymic}} </div>',
                        enableCellEdit: false
                    },
                    {
                        field: 'participant2',
                        displayName: 'Участник 2',
                        //cellTemplate: '<div class="ui-grid-cell-contents"> {{row.entity.participantByParticipant2.name}} {{row.entity.participantByParticipant2.surname}} {{row.entity.participantByParticipant2.patronymic}} </div>',
                        enableCellEdit: false
                    },
                    {
                        name: 'winner',
                        displayName: 'Победитель',
                        editableCellTemplate: 'ui-grid/dropdownEditor',
                        width: '20%',
                        enableCellEdit: $scope.judge,
                        editDropdownValueLabel: 'winner',
                        editDropdownOptionsArray: [
                            { id: 0, winner: ""},
                            { id: 1, winner: 'Участник 1' },
                            { id: 2, winner: 'Участник 2' },
                            { id: 3, winner: 'Дисквалификация' }
                        ]
                    },
                    {
                        field: 'comment',
                        displayName: 'Комментарий',
                        width: 160,
                        enableCellEdit: $scope.judge,
                    },
                    {
                        field: 'weightCategory',
                        displayName: 'Весовая категория',
                        width: 160,
                        enableCellEdit: false
                    },
                    {
                        field: 'participantByParticipant1.sex',
                        displayName: 'Пол',
                        width: 120,
                        enableCellEdit: false,
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [ { value: 'Мужской', label: 'Мужской' }, { value: 'Женский', label: 'Женский' } ]
                        }
                    },
                    {
                        name: ' ',
                        width: 25,
                        cellTemplate: '<span class="goodsIconButtons goodsIconButtonsSee glyphicon glyphicon-eye-open" ng-click="grid.appScope.buttonsClick($event, 1, row)"></span>',
                        enableFiltering: false,
                        enableSorting: false,
                        enableColumnMenu: false,
                        enableCellEdit: false
                    }
                ],
                enableGridMenu: true,
                exporterPdfDefaultStyle: {fontSize: 9},
                exporterPdfTableStyle: {margin: [5, 5, 5, 5]},
                exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                exporterPdfHeader: { text: "Турнир \"" + store.name + "\" - Текущая пулька", style: 'headerStyle' },
                exporterPdfFooter: function ( currentPage, pageCount ) {
                    return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                },
                exporterPdfCustomFormatter: function ( docDefinition ) {
                    docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                    docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                    return docDefinition;
                },
                exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                exporterCsvFilename: 'results.csv',
                exporterPdfOrientation: 'portrait',
                exporterPdfPageSize: 'LETTER'
            };
            $scope.gridOptions.onRegisterApi = function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    if(rowEntity.winner == "Участник 1" || rowEntity.winner==1){
                        rowEntity.winner = 1;
                    } else if(rowEntity.winner == "Участник 2" || rowEntity.winner==2){
                        rowEntity.winner = 2;
                    } else if(rowEntity.winner == "Дисквалификация" || rowEntity.winner==3){
                        rowEntity.winner = 3;
                    } else{
                        rowEntity.winner = 0;
                    }
                    if(rowEntity.participantByParticipant2 == null){
                        rowEntity.winner = 1;
                    }
                    $http({
                        method: 'PUT',
                        url: baseUrl + objectName + rowEntity.id,
                        data: rowEntity
                    }).then(function (data) {
                        console.log("Good update success: " + data);
                        doRequest();
                    }).catch(function (response) {
                        console.log("Good update error: " + response);
                        $rootScope.errorsHandle(response);
                        doRequest();
                    });
                });
            };

            var doRequest = function () {
                console.log("here refreshing");
                console.log(store.status);
                if (storeId != '' && store.status == "Начато") {
                    $scope.loadingGridOptions = false;
                    $scope.checkStatus = false;
                    $http({
                        method: 'GET',
                        url: baseUrl + objectName + storeId
                    }).then(function (response) {
                        var data = response.data;
                        $scope.gridOptions.data = [];
                        console.log(data);
                        if(data.length>0 && data[0].participantByParticipant1.club == "FINISHED"){
                            store.status = "Закончен";
                            $cookieStore.put('store', store);
                            location.href = "#/winners";
                        }
                        var used = [], cnt=0;
                        for(var i=0;i<data.length;i++){
                            if(used[data[i].tour]!=1){
                                used[data[i].tour]=1;
                                tours.push({value: data[i].tour, label:data[i].tour});
                            }
                            data[i].weightCategory = data[i].participantByParticipant1.weight/3 * 3 + "кг - " + (data[i].participantByParticipant1.weight/3 * 3 + 2) + "кг";
                            if(data[i].tour == data[data.length-1].tour){
                                if(data[i].participantByParticipant2 == null){
                                    data[i].canEdit = false;
                                } else{
                                    data[i].canEdit = true;
                                }
                                console.log(data[i].winner);
                                if(data[i].winner != 1 && data[i].winner != 2 && data[i].winner != 3){
                                    $scope.checkStatus = true;
                                }
                                if(data[i].winner == 1){
                                    data[i].winner = "Участник 1";
                                } else if(data[i].winner == 2){
                                    data[i].winner = "Участник 2";
                                } else if(data[i].winner == 3){
                                    data[i].winner = "Дисквалификация";
                                } else{
                                    data[i].winner = "";
                                }
                                data[i].participant1 = data[i].participantByParticipant1.name + " " +
                                    data[i].participantByParticipant1.surname + " " +
                                    data[i].participantByParticipant1.patronymic;
                                if(data[i].participantByParticipant2!=null){
                                    data[i].participant2 = data[i].participantByParticipant2.name + " " +
                                        data[i].participantByParticipant2.surname + " " +
                                        data[i].participantByParticipant2.patronymic;
                                }
                                if(data[i].participantByParticipant1.sex == "male" || data[i].participantByParticipant1.sex == "Male" || data[i].participantByParticipant1.sex == "MALE"){
                                    data[i].participantByParticipant1.sex = "Мужской";
                                } else if(data[i].participantByParticipant1.sex == "female" || data[i].participantByParticipant1.sex == "Female" || data[i].participantByParticipant1.sex == "FEMALE"){
                                    data[i].participantByParticipant1.sex = "Женский";
                                }
                                $scope.gridOptions.data.push(data[i]);
                            }
                        }
                        $scope.loadingGridOptions = true;
                    }).catch(function (response) {
                        $rootScope.errorsHandle(response);
                        $scope.loadingGridOptions = true;
                    });
                }
            };

            $scope.finishTour = function () {
                var confirm = $mdDialog.confirm()
                    .title('Вы уверены, что хотите закончить этот тур и начать следующий тур?')
                    .textContent('После Вы не сможете изменить его.')
                    .targetEvent()
                    .ok('Отмена')
                    .cancel('Закончить');

                $mdDialog.show(confirm).then(function () {
                }, function () {
                    $http({
                        method: 'POST',
                        url: baseUrl + objectName + storeId + "/startNextTour"
                    }).then(function () {
                        console.log("started next tour");
                        doRequest();
                    }).catch(function (response) {
                        $rootScope.errorsHandle(response);
                    });
                });
            };

            var getPage = function () {
                doRequest();
            };

            var changeStore = function () {
                storeId = store.id;
                if (storeId != '') {
                    $(".storesLabel").removeAttr("style");
                }

                if (storeId.length == 0) {
                    $scope.checker = true;
                } else {
                    $scope.checker = false;
                }

                getPage();
            };

            changeStore();

            $scope.importParticipants = {};

            $scope.reset = function() {
                $scope.importParticipants.data = [];
                $scope.importParticipants.columnDefs = [];
            }
            $rootScope.makeImport = function(importClub) {
                console.log($scope.importParticipants.data);
                console.log(importClub);
                var row = {};
                row.data = $scope.importParticipants.data;
                for(var i=0;i<row.data.length;i++){
                    row.data[i].club = importClub;
                }
                console.log(row);
                $http({
                    method: 'POST',
                    url: baseUrl + objectName + storeId,
                    data: row
                }).then(function (data) {
                    console.log("Good adding success: " + data);
                    $scope.refreshTable();
                }).catch(function (response) {
                    console.log("Good adding error: " + response);
                    $rootScope.errorsHandle(response);
                });
            };

            $scope.refreshTable = function(){
                console.log("refreshing");
                doRequest();
            }

        }]);