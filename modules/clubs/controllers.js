'use strict';


angular.module('Clubs', [
        'ngTouch',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.selection'
    ])

    .controller('ClubsController', ['$scope', '$http', 'configuration', '$mdDialog', '$rootScope', '$cookieStore', 'uiGridConstants', '$timeout',
        function ($scope, $http, configuration, $mdDialog, $rootScope, $cookieStore, uiGridConstants, $timeout) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'club/';
            var storeId = '';

            $scope.storesList = {};
            $scope.onlyNumbers = /^\d+$/;
            $scope.loadingGridOptions = true;

            var role = $rootScope.globals.currentUser.role;
            var selectedStore = '';

            $scope.checker = true;

            $scope.addGood = function () {
                var dataToPass = {};
                $mdDialog.show({
                    locals: {dataToPass: dataToPass},
                    controller: DialogController,
                    templateUrl: 'modules/clubs/views/add-modal.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen
                });
            };

            $rootScope.buttonsClick = function (ev, type, row) {
                var dataToPass = row.entity;

                if (type == 1) {
                    $mdDialog.show({
                        locals: {dataToPass: dataToPass},
                        controller: DialogController,
                        templateUrl: 'modules/clubs/views/display-modal.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen
                    });
                } else if (type == 2) {
                    $mdDialog.show({
                            locals: {dataToPass: dataToPass},
                            controller: DialogController,
                            templateUrl: 'modules/clubs/views/update-modal.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: false,
                            fullscreen: $scope.customFullscreen
                        })
                        .then(function (answer) {
                        }, function () {
                            doRequest();
                        });
                } else {
                    var confirm = $mdDialog.confirm()
                        .title('Вы уверены, что хотите удалить клуб "' + dataToPass.name + '"?')
                        .textContent('После удаления Вы не сможете его вернуть.')
                        .targetEvent(ev)
                        .ok('Отмена')
                        .cancel('Удалить');

                    $mdDialog.show(confirm).then(function () {
                    }, function () {
                        console.log("deleting");
                        $http({
                            method: 'DELETE',
                            url: baseUrl + objectName + "/" + dataToPass.id
                        }).then(function () {
                            //var index = $scope.gridOptions.data.indexOf(row.entity);
                            //$scope.gridOptions.data.splice(index, 1);
                            console.log("delete success");
                            doRequest();
                        }).catch(function (response) {
                            $rootScope.errorsHandle(response);
                            doRequest();
                        });
                    });
                }
            };

            function DialogController($scope, $mdDialog, dataToPass) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    if(answer == "editFromDisplay"){
                        var r = {};
                        r.entity = dataToPass;
                        $rootScope.buttonsClick(null, 2, r);
                    } else if (answer == 'update') {
                        $http({
                            method: 'PUT',
                            url: baseUrl + objectName + "/" + $scope.choosenGood.id,
                            data: $scope.choosenGood
                        }).then(function (data) {
                            console.log("Good update success: " + data);
                        }).catch(function (response) {
                            console.log("Good update error: " + response);
                            $rootScope.errorsHandle(response);
                        });
                    } else if (answer == 'not update') {
                        doRequest();
                    } else if (answer == 'add') {
                        console.log($scope.addingGood);
                        $http({
                            method: 'POST',
                            url: baseUrl + objectName,
                            data: $scope.addingGood
                        }).then(function (data) {
                            console.log("Good adding success: " + data);
                            doRequest();
                        }).catch(function (response) {
                            console.log("Good adding error: " + response);
                            $rootScope.errorsHandle(response);
                        });
                    }
                    $mdDialog.hide(answer);
                };
                $scope.choosenGood = dataToPass;
                $scope.addingGood = dataToPass;
            }

            $scope.gridOptions = {
                enableFiltering: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                multiSelect: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                columnDefs: [
                    {
                        field: 'name',
                        displayName: 'Название'
                    },
                    {
                        name: ' ',
                        width: 50,
                        cellTemplate: //'<span class="goodsIconButtons goodsIconButtonsSee glyphicon glyphicon-eye-open" ng-click="grid.appScope.buttonsClick($event, 1, row)"></span>' +
                        '<span class="goodsIconButtons glyphicon glyphicon-pencil" ng-click="grid.appScope.buttonsClick($event, 2, row)"></span>' +
                        '<span class="goodsIconButtons glyphicon glyphicon-trash" ng-click="grid.appScope.buttonsClick($event, 3, row)"></span>',
                        enableFiltering: false,
                        enableSorting: false,
                        enableColumnMenu: false
                    }
                ]
            };
            $scope.gridOptions.onRegisterApi = function (gridApi) {
                $scope.gridApi = gridApi;

                gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.checker = false;
                    selectedStore = row.entity;
                });
            };

            var doRequest = function () {
                $scope.loadingGridOptions = false;

                $http({
                    method: 'GET',
                    url: baseUrl + objectName
                }).then(function (response) {
                    var data = response.data;
                    $scope.gridOptions.data = data;
                    $scope.loadingGridOptions = true;
                }).catch(function (response) {
                    $rootScope.errorsHandle(response);
                    $scope.loadingGridOptions = true;
                });
            };
            doRequest();

        }]);
