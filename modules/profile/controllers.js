'use strict';

angular.module('Profile', ['ngTouch', 'ngMessages', 'myApp.directives'])

    .controller('ProfileController', ['$scope', '$http', '$mdDialog', 'configuration', '$cookieStore',
        function($scope, $http, $mdDialog, configuration, $cookieStore) {

            var globals = $cookieStore.get('globals') || {};
            var login = globals.currentUser.username;

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'settings/users/' + login;

            console.log(baseUrl + objectName);

            $http({
                method: 'GET',
                url: baseUrl + objectName
            }).then(function(response) {
                $scope.user = response.data;
                var date = new Date($scope.user.lastLoginDate);
                $scope.convertedDate = date.toString();
            }).catch(function(response) {
                console.log("Error on getting data: " + response);
                $rootScope.errorsHandle(response);
            });

            $scope.enabled = false

            $scope.toggleText = "Изменить";

            $scope.toggleButton = function() {

                $scope.enabled = !$scope.enabled;

                if($scope.enabled == false){
                  $scope.toggleText = "Изменить";

                  var data = {
                      "userName": $scope.user.userName,
                      "email": $scope.user.email
                  };

                  console.log(data);

                  $http({
                      method: 'PUT',
                      url: baseUrl + objectName,
                      data: data
                  }).then(function(data) {
                      console.log("Profile update success: " + data);
                  }).catch(function(response) {
                      console.log("Profile update error: " + response);
                      $rootScope.errorsHandle(response);
                  });

                }else{
                  $scope.toggleText = "Сохранить";
                }
            };

            $scope.showModal = function(ev) {

                $mdDialog.show({
                        controller: PasswordController,
                        templateUrl: 'modules/profile/views/changePassword.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    })
                    .then(function(answer) {
                        $scope.status = 'You said the information was ' + answer + '.';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };

            function PasswordController($scope, $mdDialog) {
                $scope.hide = function() {
                    $mdDialog.hide();
                };

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.answer = function(user, answer) {

                    if(answer == 'resetPassword'){

                      if(user.newPassword == user.confirmNewPassword) {

                        var data = {
                            "currentPassword": sha256(user.oldPassword),
                            "newPassword": sha256(user.newPassword)
                        };

                        console.log(data);

                        $http({
                            method: 'PUT',
                            url: baseUrl + objectName + "/new_password",
                            data: data
                        }).then(function (data) {
                            console.log("Password reset success: " + data);
                        }).catch(function (response) {
                            console.log("Password reset error: " + response);
                            $rootScope.errorsHandle(response);
                        });
                      } else {
                        console.log("Password does not match");
                      }
                    }

                    $mdDialog.hide(answer);
                };
            }
        }
    ])

    angular.module('myApp.directives', [])
        .directive('pwCheck', [function() {
            return {
                require: 'ngModel',
                link: function(scope, elem, attrs, ctrl) {
                    var firstPassword = '#' + attrs.pwCheck;
                    elem.add(firstPassword).on('keyup', function() {
                        scope.$apply(function() {
                            // console.info(elem.val() === $(firstPassword).val());
                            ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                        });
                    });
                }
            }
        }]);
