'use strict';


angular.module('Winners', [
        'ngTouch',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.selection',
        'ui.grid.edit',
        'ui.grid.cellNav',
        'ui.grid.exporter'
    ])

    .controller('WinnersController', ['$scope', '$http', 'configuration', '$mdDialog', '$rootScope', '$cookieStore', 'uiGridConstants', '$timeout',
        function ($scope, $http, configuration, $mdDialog, $rootScope, $cookieStore, uiGridConstants, $timeout) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'games/';
            var storeId = '';
            var sexx = ["Мужской","Женский"];
            var settings = $cookieStore.get('settings') || {};
            var store = $cookieStore.get('store') || {};
            var clubs = [];

            $scope.checkStatus = false;
            if(store.status == "Новый"){
                $scope.checkStatus = true;
            }

            $scope.storesList = {};
            $scope.onlyNumbers = /^\d+$/;
            $scope.loadingGridOptions = true;

            $rootScope.buttonsClick = function (ev, type, row) {
                var dataToPass = row.entity;

                if (type == 1) {
                    $mdDialog.show({
                        locals: {dataToPass: dataToPass},
                        controller: DialogController,
                        templateUrl: 'modules/winners/views/display-modal.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false,
                        fullscreen: $scope.customFullscreen
                    });
                }
            };

            function DialogController($scope, $mdDialog, dataToPass) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    if(answer == "editFromDisplay"){
                        var r = {};
                        r.entity = dataToPass;
                        $rootScope.buttonsClick(null, 2, r);
                    } else if (answer == 'update') {
                        for(var i=0;i<clubs.length;i++){
                            if(clubs[i].name == $scope.choosenGood.club){
                                $scope.choosenGood.club = clubs[i].id;
                            }
                        }
                        $http({
                            method: 'PUT',
                            url: baseUrl + objectName + "/" + $scope.choosenGood.id,
                            data: $scope.choosenGood
                        }).then(function (data) {
                            console.log("Good update success: " + data);
                            doRequest();
                        }).catch(function (response) {
                            console.log("Good update error: " + response);
                            $rootScope.errorsHandle(response);
                            doRequest();
                        });
                    } else if (answer == 'not update') {
                        doRequest();
                    } else if (answer == 'add') {
                        console.log($scope.choosenGood);
                        var row = {};
                        row.data = [];
                        row.data[0] = $scope.choosenGood;
                        console.log(row);
                        $http({
                            method: 'POST',
                            url: baseUrl + objectName + storeId,
                            data: row
                        }).then(function (data) {
                            console.log("Good adding success: " + data);
                            doRequest();
                        }).catch(function (response) {
                            console.log("Good adding error: " + response);
                            $rootScope.errorsHandle(response);
                        });
                    }  else if (answer == 'import') {
                        $rootScope.makeImport(dataToPass.importClub);
                    }
                    $mdDialog.hide(answer);
                };
                $scope.choosenGood = dataToPass;
                $scope.addingGood = dataToPass;
            }
            $scope.rowDblClick = function( row) {
                $rootScope.buttonsClick(null, 1, row);
            };

            $scope.gridOptions = {
                enableFiltering: true,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                paginationPageSizes: [25, 50, 75],
                paginationPageSize: 25,
                multiSelect: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                enableColumnResizing: true,
                columnDefs: [
                    {
                        field: 'participant',
                        displayName: 'Участник',
                        //cellTemplate: '<div class="ui-grid-cell-contents"> {{row.entity.participantByParticipant2.name}} {{row.entity.participantByParticipant2.surname}} {{row.entity.participantByParticipant2.patronymic}} </div>',
                        enableCellEdit: false
                    },
                    {
                        field: 'place',
                        displayName: 'Место',
                        enableCellEdit: false,
                        width: 100
                    },
                    {
                        field: 'weightCategory',
                        displayName: 'Весовая категория',
                        width: 160,
                        enableCellEdit: false
                    },
                    {
                        field: 'sex',
                        displayName: 'Пол',
                        width: 120,
                        enableCellEdit: false,
                        filter: {
                            type: uiGridConstants.filter.SELECT,
                            selectOptions: [ { value: 'Мужской', label: 'Мужской' }, { value: 'Женский', label: 'Женский' } ]
                        }
                    },
                    {
                        name: ' ',
                        width: 25,
                        cellTemplate: '<span class="goodsIconButtons goodsIconButtonsSee glyphicon glyphicon-eye-open" ng-click="grid.appScope.buttonsClick($event, 1, row)"></span>',
                        enableFiltering: false,
                        enableSorting: false,
                        enableColumnMenu: false,
                        enableCellEdit: false
                    }
                ],
                enableGridMenu: true,
                exporterPdfDefaultStyle: {fontSize: 9},
                exporterPdfTableStyle: {margin: [5, 5, 5, 5]},
                exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                exporterPdfHeader: { text: "Турнир \"" + store.name + "\" - Победители", style: 'headerStyle' },
                exporterPdfFooter: function ( currentPage, pageCount ) {
                    return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                },
                exporterPdfCustomFormatter: function ( docDefinition ) {
                    docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                    docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                    return docDefinition;
                },
                exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                exporterCsvFilename: 'results.csv',
                exporterPdfOrientation: 'portrait',
                exporterPdfPageSize: 'LETTER'
            };
            $scope.gridOptions.onRegisterApi = function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    console.log(rowEntity);
                    for(var i=0;i<clubs.length;i++){
                        if(clubs[i].name == rowEntity.club){
                            rowEntity.club = clubs[i].id;
                        }
                    }
                    $http({
                        method: 'PUT',
                        url: baseUrl + objectName + rowEntity.id,
                        data: rowEntity
                    }).then(function (data) {
                        console.log("Good update success: " + data);
                        doRequest();
                    }).catch(function (response) {
                        console.log("Good update error: " + response);
                        $rootScope.errorsHandle(response);
                        doRequest();
                    });
                });
            };

            var doRequest = function () {
                console.log("here refreshing");
                if (storeId != '') {
                    console.log("her1 refreshing");
                    $scope.loadingGridOptions = false;
                    $http({
                        method: 'GET',
                        url: baseUrl + objectName + storeId + "/getWinners"
                    }).then(function (response) {
                        var data = response.data;
                        console.log(data);
                        var place = [];
                        var place1 = [];
                        for(var i=0;i<data.length;i++) {
                            data[i].weightCategory = data[i].weight / 3 * 3 + "кг - " + (data[i].weight / 3 * 3 + 2) + "кг";
                            place[data[i].weightCategory] = 0;
                            place1[data[i].weightCategory] = 0;
                        }
                        for(var i=0;i<data.length;i++) {
                            data[i].weightCategory = data[i].weight / 3 * 3 + "кг - " + (data[i].weight / 3 * 3 + 2) + "кг";
                            data[i].participant = data[i].name + " " +
                                data[i].surname + " " +
                                data[i].patronymic;
                            if (data[i].sex == "male" || data[i].sex == "Male" || data[i].sex == "MALE") {
                                data[i].sex = "Мужской";
                            } else if (data[i].sex == "female" || data[i].sex == "Female" || data[i].sex == "FEMALE") {
                                data[i].sex = "Женский";
                            }
                            if(data[i].sex=="Мужской"){
                                data[i].place = ++place[data[i].weightCategory];
                            } else{
                                data[i].place = ++place1[data[i].weightCategory];
                            }
                        }
                        $scope.gridOptions.data = data;
                        $scope.loadingGridOptions = true;
                    }).catch(function (response) {
                        $rootScope.errorsHandle(response);
                        $scope.loadingGridOptions = true;
                    });
                }
            };

            var getPage = function () {
                $http({
                    method: 'GET',
                    url: baseUrl + "club"
                }).then(function (response) {
                    var data = response.data;
                    clubs = data;
                    doRequest();
                }).catch(function (response) {
                    $rootScope.errorsHandle(response);
                    $scope.loadingGridOptions = true;
                });
            };

            var changeStore = function () {
                storeId = store.id;
                if (storeId != '') {
                    $(".storesLabel").removeAttr("style");
                }

                if (storeId.length == 0) {
                    $scope.checker = true;
                } else {
                    $scope.checker = false;
                }

                getPage();
            };
            changeStore();
        }]);