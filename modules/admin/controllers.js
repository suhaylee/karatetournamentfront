'user_strict';

angular.module('Admin', ['ngTouch'])

    .controller('AdminController', ['$scope', '$http', 'configuration', '$cookieStore', '$rootScope', '$mdDialog',
        function($scope, $http, configuration, $cookieStore, $rootScope, $mdDialog) {

            var baseUrl = configuration.apiBaseUrl;
            var objectName = 'stores/deleteRedundantStores';

            var role = $cookieStore.get('globals') || null;
            if(role!=null){
                role = role.currentUser.role;
                $scope.showLogin = false;
                $scope.showLogout = true;
            } else{
                $scope.showLogin = true;
                $scope.showLogout = false;
            }
            if (role === "ADMIN") {
                $scope.admin = true;
                $scope.judge = false;
                $rootScope.admin = true;
                $rootScope.judge = false;
            } else if (role === "JUDGE"){
                $scope.admin = false;
                $scope.judge = true;
                $rootScope.admin = false;
                $rootScope.judge = true;
            } else{
                $scope.admin = false;
                $scope.judge = false;
                $rootScope.admin = false;
                $rootScope.judge = false;
            }

            $scope.checkRole = false;
            if (role === "ADMIN") {
                $scope.checkRole = true;
            } else if (role === "OWNER"){
                $scope.checkRole = false;
            }

            $scope.showConfirm = function(ev) {

                var confirm = $mdDialog.confirm()
                    .title('Вы действительно хотите удалить все ненужные данные?')
                    .targetEvent(ev)
                    .ok('Да')
                    .cancel('Нет');

                $mdDialog.show(confirm).then(function() {
                    $scope.status = 'You decided to delete.';

                    $http({
                        method: 'POST',
                        url: baseUrl + objectName
                    }).then(function(response) {
                        console.log(response);
                    }).catch(function(response) {
                        console.log("Error on getting data: " + response);
                        $rootScope.errorsHandle(response);
                    });

                }, function() {
                    $scope.status = 'You refused to delete.';
                });

                console.log($scope.status);
            };
        }
    ]);